package poopoodice.fparmstransform;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.player.ClientPlayerEntity;
import net.minecraft.client.renderer.FirstPersonRenderer;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.PlayerRenderer;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.Hand;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.vector.Vector3f;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.client.event.RenderHandEvent;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;

import java.util.Arrays;

import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_LEFT_HAND;
import static net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType.FIRST_PERSON_RIGHT_HAND;
import static poopoodice.fparmstransform.Config.*;

@Mod.EventBusSubscriber(Dist.CLIENT)
public class ClientForgeEventBus
{
    @SubscribeEvent
    public static void onClientTick(TickEvent.ClientTickEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        PlayerEntity player = minecraft.player;
        if (event.phase == TickEvent.Phase.START && player != null && !(minecraft.currentScreen instanceof TransformScreen) && ENABLED.get())
            minecraft.displayGuiScreen(new TransformScreen());
    }

    @SubscribeEvent
    public static void onHandsRender(RenderHandEvent event)
    {
        Minecraft minecraft = Minecraft.getInstance();
        ClientPlayerEntity player = minecraft.player;
        if (player != null && player.isAlive() && ENABLED.get())
        {
            PlayerRenderer renderer = (PlayerRenderer) minecraft.getRenderManager().getRenderer(player);
            MatrixStack stack = event.getMatrixStack();
            HandSide hand = event.getHand() == Hand.MAIN_HAND ? HandSide.RIGHT : HandSide.LEFT;
            event.setCanceled(true);
            if (shouldRender(hand, true))
            {
                stack.push();
                stack.translate(0.0D, -0.46F, -0.32F);
                stack.rotate(Vector3f.XP.rotationDegrees(-85.0F));
                stack.rotate(Vector3f.YP.rotationDegrees(180.0F));
                stack.translate(getTranslation(hand).x, getTranslation(hand).y, getTranslation(hand).z);
                stack.rotate(Vector3f.XP.rotationDegrees((float) getRotation(hand).x));
                stack.rotate(Vector3f.YP.rotationDegrees((float) getRotation(hand).y));
                stack.rotate(Vector3f.ZP.rotationDegrees((float) getRotation(hand).z));
                stack.scale((float) getScale(hand).x, (float) getScale(hand).y, (float) getScale(hand).z);
                if (hand == HandSide.LEFT)
                    renderer.renderLeftArm(stack, event.getBuffers(), getLightForHand(hand, event.getLight()), player);
                else renderer.renderRightArm(stack, event.getBuffers(), getLightForHand(hand, event.getLight()), player);
                stack.pop();
            }
            if (shouldRender(hand, false))
            {
                stack.push();
                stack.translate(getItemTranslation(hand).x, getItemTranslation(hand).y, getItemTranslation(hand).z);
                stack.rotate(Vector3f.XP.rotationDegrees((float) getItemRotation(hand).x));
                stack.rotate(Vector3f.YP.rotationDegrees((float) getItemRotation(hand).y));
                stack.rotate(Vector3f.ZP.rotationDegrees((float) getItemRotation(hand).z));
                stack.scale((float) getItemScale(hand).x, (float) getItemScale(hand).y, (float) getItemScale(hand).z);
                renderItem(player, minecraft.getFirstPersonRenderer(), hand, stack, event.getBuffers(), getLightForItem(hand, event.getLight()), getItemStack(hand));
                stack.pop();
            }
        }
    }

    private static void renderItem(ClientPlayerEntity player, FirstPersonRenderer renderer, HandSide hand, MatrixStack stack, IRenderTypeBuffer buffer, int light, ItemStack item)
    {
        boolean main = hand == HandSide.RIGHT;
        int i = main ? 1 : -1;
        stack.push();
        stack.translate(0.56F * i, -0.52F, -0.72F);
        renderer.renderItemSide(player, item, main ? FIRST_PERSON_RIGHT_HAND : FIRST_PERSON_LEFT_HAND, !main, stack, buffer, light);
        stack.pop();
    }

    private static int getLightForHand(HandSide hand, int light)
    {
        return Arrays.stream(HANDS).anyMatch((side) -> hand == side) ? light : light * 2;
    }

    private static int getLightForItem(HandSide hand, int light)
    {
        return (hand == HandSide.LEFT && LEFT_ITEM) || (hand == HandSide.RIGHT && RIGHT_ITEM) ? light : light * 2;
    }

    @SubscribeEvent
    public static void onOverlayRender(RenderGameOverlayEvent.Pre event)
    {
        if (event.getType() == RenderGameOverlayEvent.ElementType.HOTBAR && ENABLED.get())
            event.setCanceled(true);
    }
}
