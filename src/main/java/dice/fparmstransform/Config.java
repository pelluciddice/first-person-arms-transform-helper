package poopoodice.fparmstransform;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.HandSide;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraftforge.common.ForgeConfigSpec;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.UUID;

public class Config
{
    public static ForgeConfigSpec.BooleanValue ENABLED;

    public static ForgeConfigSpec.BooleanValue LEFT_HAND_VISIBILITY;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_TRANSLATION_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_TRANSLATION_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_TRANSLATION_Z;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ROTATION_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ROTATION_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ROTATION_Z;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_SCALE_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_SCALE_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_SCALE_Z;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_PERSPECTIVE;

    public static ForgeConfigSpec.BooleanValue LEFT_HAND_ITEM_VISIBILITY;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG_2;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG_3;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG_4;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG_5;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_TAG_6;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_TRANSLATION_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_TRANSLATION_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_TRANSLATION_Z;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_ROTATION_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_ROTATION_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_ROTATION_Z;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_SCALE_X;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_SCALE_Y;
    public static ForgeConfigSpec.DoubleValue LEFT_HAND_ITEM_SCALE_Z;
    public static ForgeConfigSpec.ConfigValue<String> LEFT_HAND_ITEM_PERSPECTIVE;

    public static ForgeConfigSpec.BooleanValue RIGHT_HAND_VISIBILITY;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_TRANSLATION_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_TRANSLATION_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_TRANSLATION_Z;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ROTATION_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ROTATION_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ROTATION_Z;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_SCALE_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_SCALE_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_SCALE_Z;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_PERSPECTIVE;

    public static ForgeConfigSpec.BooleanValue RIGHT_HAND_ITEM_VISIBILITY;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG_2;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG_3;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG_4;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG_5;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_TAG_6;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_TRANSLATION_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_TRANSLATION_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_TRANSLATION_Z;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_ROTATION_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_ROTATION_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_ROTATION_Z;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_SCALE_X;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_SCALE_Y;
    public static ForgeConfigSpec.DoubleValue RIGHT_HAND_ITEM_SCALE_Z;
    public static ForgeConfigSpec.ConfigValue<String> RIGHT_HAND_ITEM_PERSPECTIVE;

    public static Transforms TRANSFORM = Transforms.TRANSLATION;
    public static HandSide[] HANDS = new HandSide[2];
    public static boolean LEFT_ITEM = false;
    public static boolean RIGHT_ITEM = false;
    public static Modes MODE = Modes.HAND;
    public static boolean INFO = true;

    public static ForgeConfigSpec build(ForgeConfigSpec.Builder builder)
    {
        ENABLED = builder.define("enabled", true);
        makeSpace(builder);
        LEFT_HAND_VISIBILITY = builder.define("lefthand_visibility", true);
        LEFT_HAND_TRANSLATION_X = builder.defineInRange("lefthand_translation_x", 0.0F, -10, 10);
        LEFT_HAND_TRANSLATION_Y =  builder.defineInRange("lefthand_translation_y", 0.0F, -10, 10);
        LEFT_HAND_TRANSLATION_Z = builder.defineInRange("lefthand_translation_z", 0.0F, -10, 10);
        LEFT_HAND_ROTATION_X = builder.defineInRange("lefthand_rotation_x", 0.0F, -360, 360);
        LEFT_HAND_ROTATION_Y = builder.defineInRange("lefthand_rotation_y", 0.0F, -360, 360);
        LEFT_HAND_ROTATION_Z = builder.defineInRange("lefthand_rotation_z", 0.0F, -360, 360);
        LEFT_HAND_SCALE_X = builder.defineInRange("lefthand_scale_x", 1.0F, -10, 10);
        LEFT_HAND_SCALE_Y = builder.defineInRange("lefthand_scale_y", 1.0F, -10, 10);
        LEFT_HAND_SCALE_Z = builder.defineInRange("lefthand_scale_z", 1.0F, -10, 10);
        LEFT_HAND_PERSPECTIVE = builder.define("left_hand_perspective", "");

        commentDataTypes(builder);
        LEFT_HAND_ITEM_VISIBILITY = builder.define("lefthand_item_visibility", true);
        LEFT_HAND_ITEM = builder.define("lefthand_item", "minecraft:air");
        LEFT_HAND_ITEM_TAG = builder.define("lefthand_item_tag", "");
        LEFT_HAND_ITEM_TAG_2 = builder.define("lefthand_item_tag_2", "");
        LEFT_HAND_ITEM_TAG_3 = builder.define("lefthand_item_tag_3", "");
        LEFT_HAND_ITEM_TAG_4 = builder.define("lefthand_item_tag_4", "");
        LEFT_HAND_ITEM_TAG_5 = builder.define("lefthand_item_tag_5", "");
        LEFT_HAND_ITEM_TAG_6 = builder.define("lefthand_item_tag_6", "");
        LEFT_HAND_ITEM_TRANSLATION_X = builder.defineInRange("lefthand_item_translation_x", 0.0F, -10, 10);
        LEFT_HAND_ITEM_TRANSLATION_Y =  builder.defineInRange("lefthand_item_translation_y", 0.0F, -10, 10);
        LEFT_HAND_ITEM_TRANSLATION_Z = builder.defineInRange("lefthand_item_translation_z", 0.0F, -10, 10);
        LEFT_HAND_ITEM_ROTATION_X = builder.defineInRange("lefthand_item_rotation_x", 0.0F, -360, 360);
        LEFT_HAND_ITEM_ROTATION_Y = builder.defineInRange("lefthand_item_rotation_y", 0.0F, -360, 360);
        LEFT_HAND_ITEM_ROTATION_Z = builder.defineInRange("lefthand_item_rotation_z", 0.0F, -360, 360);
        LEFT_HAND_ITEM_SCALE_X = builder.defineInRange("lefthand_item_scale_x", 1.0F, -10, 10);
        LEFT_HAND_ITEM_SCALE_Y = builder.defineInRange("lefthand_item_scale_y", 1.0F, -10, 10);
        LEFT_HAND_ITEM_SCALE_Z = builder.defineInRange("lefthand_item_scale_z", 1.0F, -10, 10);
        LEFT_HAND_ITEM_PERSPECTIVE = builder.define("left_hand_item_perspective", "");

        makeSpace(builder);
        RIGHT_HAND_VISIBILITY = builder.define("righthand_visibility", true);
        RIGHT_HAND_TRANSLATION_X = builder.defineInRange("righthand_translation_x", 0.0F, -10, 10);
        RIGHT_HAND_TRANSLATION_Y =  builder.defineInRange("righthand_translation_y", 0.0F, -10, 10);
        RIGHT_HAND_TRANSLATION_Z = builder.defineInRange("righthand_translation_z", 0.0F, -10, 10);
        RIGHT_HAND_ROTATION_X = builder.defineInRange("righthand_rotation_x", 0.0F, -360, 360);
        RIGHT_HAND_ROTATION_Y = builder.defineInRange("righthand_rotation_y", 0.0F, -360, 360);
        RIGHT_HAND_ROTATION_Z = builder.defineInRange("righthand_rotation_z", 0.0F, -360, 360);
        RIGHT_HAND_SCALE_X = builder.defineInRange("righthand_scale_x", 1.0F, -10, 10);
        RIGHT_HAND_SCALE_Y = builder.defineInRange("righthand_scale_y", 1.0F, -10, 10);
        RIGHT_HAND_SCALE_Z = builder.defineInRange("righthand_scale_z", 1.0F, -10, 10);
        RIGHT_HAND_PERSPECTIVE = builder.define("right_hand_perspective", "");

        commentDataTypes(builder);
        RIGHT_HAND_ITEM_VISIBILITY = builder.define("righthand_item_visibility", true);
        RIGHT_HAND_ITEM = builder.define("righthand_item", "minecraft:air");
        RIGHT_HAND_ITEM_TAG = builder.define("righthand_item_tag", "");
        RIGHT_HAND_ITEM_TAG_2 = builder.define("righthand_item_tag_2", "");
        RIGHT_HAND_ITEM_TAG_3 = builder.define("righthand_item_tag_3", "");
        RIGHT_HAND_ITEM_TAG_4 = builder.define("righthand_item_tag_4", "");
        RIGHT_HAND_ITEM_TAG_5 = builder.define("righthand_item_tag_5", "");
        RIGHT_HAND_ITEM_TAG_6 = builder.define("righthand_item_tag_6", "");
        RIGHT_HAND_ITEM_TRANSLATION_X = builder.defineInRange("righthand_item_translation_x", 0.0F, -10, 10);
        RIGHT_HAND_ITEM_TRANSLATION_Y =  builder.defineInRange("righthand_item_translation_y", 0.0F, -10, 10);
        RIGHT_HAND_ITEM_TRANSLATION_Z = builder.defineInRange("righthand_item_translation_z", 0.0F, -10, 10);
        RIGHT_HAND_ITEM_ROTATION_X = builder.defineInRange("righthand_item_rotation_x", 0.0F, -360, 360);
        RIGHT_HAND_ITEM_ROTATION_Y = builder.defineInRange("righthand_item_rotation_y", 0.0F, -360, 360);
        RIGHT_HAND_ITEM_ROTATION_Z = builder.defineInRange("righthand_item_rotation_z", 0.0F, -360, 360);
        RIGHT_HAND_ITEM_SCALE_X = builder.defineInRange("righthand_item_scale_x", 1.0F, -10, 10);
        RIGHT_HAND_ITEM_SCALE_Y = builder.defineInRange("righthand_item_scale_y", 1.0F, -10, 10);
        RIGHT_HAND_ITEM_SCALE_Z = builder.defineInRange("righthand_item_scale_z", 1.0F, -10, 10);
        RIGHT_HAND_ITEM_PERSPECTIVE = builder.define("right_hand_item_perspective", "");
        return builder.build();
    }

    private static void makeSpace(ForgeConfigSpec.Builder builder)
    {
        builder.comment(" ", " ");
    }

    private static void commentDataTypes(ForgeConfigSpec.Builder builder)
    {
        builder.comment(" ", " ", " Tag Format -> type:key:value, 0 for boolean, 1 for double, 2 for float,",
                " 3 for int, 4 for short, 5 for long, 6 for string, 7 for uuid",
                " Example -> 3:phase:5 -> compound.putInt(\"phase\", 5})",
                " ");
    }

    public static boolean shouldRender(HandSide hand, boolean arms)
    {
        if (arms)
            return (hand == HandSide.LEFT && LEFT_HAND_VISIBILITY.get()) || (hand == HandSide.RIGHT && RIGHT_HAND_VISIBILITY.get());
        return (hand == HandSide.LEFT && LEFT_HAND_ITEM_VISIBILITY.get()) || (hand == HandSide.RIGHT && RIGHT_HAND_ITEM_VISIBILITY.get());
    }

    public static ItemStack getItemStack(HandSide hand)
    {
        boolean left = hand == HandSide.LEFT;
        ItemStack stack = new ItemStack(ForgeRegistries.ITEMS.getValue(new ResourceLocation(left ? LEFT_HAND_ITEM.get() : RIGHT_HAND_ITEM.get())));
        CompoundNBT compound = new CompoundNBT();
        if (left)
        {
            put(compound, LEFT_HAND_ITEM_TAG);
            put(compound, LEFT_HAND_ITEM_TAG_2);
            put(compound, LEFT_HAND_ITEM_TAG_3);
            put(compound, LEFT_HAND_ITEM_TAG_4);
            put(compound, LEFT_HAND_ITEM_TAG_5);
            put(compound, LEFT_HAND_ITEM_TAG_6);
        }
        else
        {
            put(compound, RIGHT_HAND_ITEM_TAG);
            put(compound, RIGHT_HAND_ITEM_TAG_2);
            put(compound, RIGHT_HAND_ITEM_TAG_3);
            put(compound, RIGHT_HAND_ITEM_TAG_4);
            put(compound, RIGHT_HAND_ITEM_TAG_5);
            put(compound, RIGHT_HAND_ITEM_TAG_6);
        }
        stack.setTag(compound);
        return stack;
    }

    private static void put(CompoundNBT compound, ForgeConfigSpec.ConfigValue<String> config)
    {
        String tag = config.get();
        if (tag.isEmpty()) return;
        String[] tags = tag.split(":");
        if (tags.length != 3)
        {
            FPArmsTransform.LOGGER.error(config.get() + " is not formatted correctly");
            config.set("");
            return;
        }
        int type = Integer.parseInt(tags[0]);
        String key = tags[1];
        String value = tags[2];
        try
        {
            switch (type)
            {
                case 0:
                    compound.putBoolean(key, Boolean.parseBoolean(value));
                    break;
                case 1:
                    compound.putDouble(key, Double.parseDouble(value));
                    break;
                case 2:
                    compound.putFloat(key, Float.parseFloat(value));
                    break;
                case 3:
                    compound.putInt(key, Integer.parseInt(value));
                    break;
                case 4:
                    compound.putShort(key, Short.parseShort(value));
                    break;
                case 5:
                    compound.putLong(key, Long.parseLong(value));
                    break;
                case 6:
                    compound.putString(key, value);
                    break;
                case 7:
                    compound.putUniqueId(key, UUID.fromString(value));
                    break;
                default:
                    FPArmsTransform.LOGGER.error(type + " is not a valid type" + " --> " + config.get());
                    config.set("");
                    break;
            }
        }
        catch (Exception e)
        {
            FPArmsTransform.LOGGER.error("value " + value + " cannon be cast to type " + type + " --> " + config.get());
            config.set("");
        }
    }

    public static Vector3d getTranslation(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_TRANSLATION_X.get(), LEFT_HAND_TRANSLATION_Y.get(), LEFT_HAND_TRANSLATION_Z.get());
        return new Vector3d(RIGHT_HAND_TRANSLATION_X.get(), RIGHT_HAND_TRANSLATION_Y.get(), RIGHT_HAND_TRANSLATION_Z.get());
    }

    public static Vector3d getRotation(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_ROTATION_X.get(), LEFT_HAND_ROTATION_Y.get(), LEFT_HAND_ROTATION_Z.get());
        return new Vector3d(RIGHT_HAND_ROTATION_X.get(), RIGHT_HAND_ROTATION_Y.get(), RIGHT_HAND_ROTATION_Z.get());
    }

    public static Vector3d getScale(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_SCALE_X.get(), LEFT_HAND_SCALE_Y.get(), LEFT_HAND_SCALE_Z.get());
        return new Vector3d(RIGHT_HAND_SCALE_X.get(), RIGHT_HAND_SCALE_Y.get(), RIGHT_HAND_SCALE_Z.get());
    }

    public static Vector3d getItemTranslation(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_ITEM_TRANSLATION_X.get(), LEFT_HAND_ITEM_TRANSLATION_Y.get(), LEFT_HAND_ITEM_TRANSLATION_Z.get());
        return new Vector3d(RIGHT_HAND_ITEM_TRANSLATION_X.get(), RIGHT_HAND_ITEM_TRANSLATION_Y.get(), RIGHT_HAND_ITEM_TRANSLATION_Z.get());
    }

    public static Vector3d getItemRotation(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_ITEM_ROTATION_X.get(), LEFT_HAND_ITEM_ROTATION_Y.get(), LEFT_HAND_ITEM_ROTATION_Z.get());
        return new Vector3d(RIGHT_HAND_ITEM_ROTATION_X.get(), RIGHT_HAND_ITEM_ROTATION_Y.get(), RIGHT_HAND_ITEM_ROTATION_Z.get());
    }

    public static Vector3d getItemScale(HandSide hand)
    {
        if (hand == HandSide.LEFT)
            return new Vector3d(LEFT_HAND_ITEM_SCALE_X.get(), LEFT_HAND_ITEM_SCALE_Y.get(), LEFT_HAND_ITEM_SCALE_Z.get());
        return new Vector3d(RIGHT_HAND_ITEM_SCALE_X.get(), RIGHT_HAND_ITEM_SCALE_Y.get(), RIGHT_HAND_ITEM_SCALE_Z.get());
    }

    public static void addTranslation(HandSide[] hands, double x, double y, double z)
    {
        if (hands == null)
        {
            if (LEFT_ITEM)
            {
                LEFT_HAND_ITEM_TRANSLATION_X.set(round(LEFT_HAND_ITEM_TRANSLATION_X.get() + x));
                LEFT_HAND_ITEM_TRANSLATION_Y.set(round(LEFT_HAND_ITEM_TRANSLATION_Y.get() + y));
                LEFT_HAND_ITEM_TRANSLATION_Z.set(round(LEFT_HAND_ITEM_TRANSLATION_Z.get() + z));
            }
            if (RIGHT_ITEM)
            {
                RIGHT_HAND_ITEM_TRANSLATION_X.set(round(RIGHT_HAND_ITEM_TRANSLATION_X.get() + x));
                RIGHT_HAND_ITEM_TRANSLATION_Y.set(round(RIGHT_HAND_ITEM_TRANSLATION_Y.get() + y));
                RIGHT_HAND_ITEM_TRANSLATION_Z.set(round(RIGHT_HAND_ITEM_TRANSLATION_Z.get() + z));
            }
        }
        else
        {
            for (HandSide side : hands)
            {
                if (side == HandSide.LEFT)
                {
                    LEFT_HAND_TRANSLATION_X.set(round(LEFT_HAND_TRANSLATION_X.get() + x));
                    LEFT_HAND_TRANSLATION_Y.set(round(LEFT_HAND_TRANSLATION_Y.get() + y));
                    LEFT_HAND_TRANSLATION_Z.set(round(LEFT_HAND_TRANSLATION_Z.get() + z));
                }
                else if (side == HandSide.RIGHT)
                {
                    RIGHT_HAND_TRANSLATION_X.set(round(RIGHT_HAND_TRANSLATION_X.get() + x));
                    RIGHT_HAND_TRANSLATION_Y.set(round(RIGHT_HAND_TRANSLATION_Y.get() + y));
                    RIGHT_HAND_TRANSLATION_Z.set(round(RIGHT_HAND_TRANSLATION_Z.get() + z));
                }
            }
        }
    }

    public static void addRotation(HandSide[] hands, double x, double y, double z)
    {
        if (hands == null)
        {
            if (LEFT_ITEM)
            {
                LEFT_HAND_ITEM_ROTATION_X.set(round(LEFT_HAND_ITEM_ROTATION_X.get() + x));
                LEFT_HAND_ITEM_ROTATION_Y.set(round(LEFT_HAND_ITEM_ROTATION_Y.get() + y));
                LEFT_HAND_ITEM_ROTATION_Z.set(round(LEFT_HAND_ITEM_ROTATION_Z.get() + z));
            }
            if (RIGHT_ITEM)
            {
                RIGHT_HAND_ITEM_ROTATION_X.set(round(RIGHT_HAND_ITEM_ROTATION_X.get() + x));
                RIGHT_HAND_ITEM_ROTATION_Y.set(round(RIGHT_HAND_ITEM_ROTATION_Y.get() + y));
                RIGHT_HAND_ITEM_ROTATION_Z.set(round(RIGHT_HAND_ITEM_ROTATION_Z.get() + z));
            }
        }
        else
        {
            for (HandSide side : hands)
            {
                if (side == HandSide.LEFT)
                {
                    LEFT_HAND_ROTATION_X.set(round(LEFT_HAND_ROTATION_X.get() + x));
                    LEFT_HAND_ROTATION_Y.set(round(LEFT_HAND_ROTATION_Y.get() + y));
                    LEFT_HAND_ROTATION_Z.set(round(LEFT_HAND_ROTATION_Z.get() + z));
                }
                else if (side == HandSide.RIGHT)
                {
                    RIGHT_HAND_ROTATION_X.set(round(RIGHT_HAND_ROTATION_X.get() + x));
                    RIGHT_HAND_ROTATION_Y.set(round(RIGHT_HAND_ROTATION_Y.get() + y));
                    RIGHT_HAND_ROTATION_Z.set(round(RIGHT_HAND_ROTATION_Z.get() + z));
                }
            }
        }
    }

    public static void addScale(HandSide[] hands, double x, double y, double z)
    {
        if (hands == null)
        {
            if (LEFT_ITEM)
            {
                LEFT_HAND_ITEM_SCALE_X.set(round(LEFT_HAND_ITEM_SCALE_X.get() + x));
                LEFT_HAND_ITEM_SCALE_Y.set(round(LEFT_HAND_ITEM_SCALE_Y.get() + y));
                LEFT_HAND_ITEM_SCALE_Z.set(round(LEFT_HAND_ITEM_SCALE_Z.get() + z));
            }
            if (RIGHT_ITEM)
            {
                RIGHT_HAND_ITEM_SCALE_X.set(round(RIGHT_HAND_ITEM_SCALE_X.get() + x));
                RIGHT_HAND_ITEM_SCALE_Y.set(round(RIGHT_HAND_ITEM_SCALE_Y.get() + y));
                RIGHT_HAND_ITEM_SCALE_Z.set(round(RIGHT_HAND_ITEM_SCALE_Z.get() + z));
            }
        }
        else
        {
            for (HandSide side : hands)
            {
                if (side == HandSide.LEFT)
                {
                    LEFT_HAND_SCALE_X.set(round(LEFT_HAND_SCALE_X.get() + x));
                    LEFT_HAND_SCALE_Y.set(round(LEFT_HAND_SCALE_Y.get() + y));
                    LEFT_HAND_SCALE_Z.set(round(LEFT_HAND_SCALE_Z.get() + z));
                }
                else if (side == HandSide.RIGHT)
                {
                    RIGHT_HAND_SCALE_X.set(round(RIGHT_HAND_SCALE_X.get() + x));
                    RIGHT_HAND_SCALE_Y.set(round(RIGHT_HAND_SCALE_Y.get() + y));
                    RIGHT_HAND_SCALE_Z.set(round(RIGHT_HAND_SCALE_Z.get() + z));
                }
            }
        }
    }

    private static double round(double num)
    {
        return Math.round(num * 1000.0D) / 1000.0D;
    }

    public static void setToDefault(HandSide[] hands)
    {
        if (hands == null)
        {
            if (LEFT_ITEM)
            {
                LEFT_HAND_ITEM_TRANSLATION_X.set(0.0D);
                LEFT_HAND_ITEM_TRANSLATION_Y.set(0.0D);
                LEFT_HAND_ITEM_TRANSLATION_Z.set(0.0D);
                LEFT_HAND_ITEM_ROTATION_X.set(0.0D);
                LEFT_HAND_ITEM_ROTATION_Y.set(0.0D);
                LEFT_HAND_ITEM_ROTATION_Z.set(0.0D);
                LEFT_HAND_ITEM_SCALE_X.set(1.0D);
                LEFT_HAND_ITEM_SCALE_Y.set(1.0D);
                LEFT_HAND_ITEM_SCALE_Z.set(1.0D);
            }
            if (RIGHT_ITEM)
            {
                RIGHT_HAND_ITEM_TRANSLATION_X.set(0.0D);
                RIGHT_HAND_ITEM_TRANSLATION_Y.set(0.0D);
                RIGHT_HAND_ITEM_TRANSLATION_Z.set(0.0D);
                RIGHT_HAND_ITEM_ROTATION_X.set(0.0D);
                RIGHT_HAND_ITEM_ROTATION_Y.set(0.0D);
                RIGHT_HAND_ITEM_ROTATION_Z.set(0.0D);
                RIGHT_HAND_ITEM_SCALE_X.set(1.0D);
                RIGHT_HAND_ITEM_SCALE_Y.set(1.0D);
                RIGHT_HAND_ITEM_SCALE_Z.set(1.0D);
            }
        }
        else
        {
            for (HandSide side : hands)
            {
                if (side == HandSide.LEFT)
                {
                    LEFT_HAND_TRANSLATION_X.set(0.0D);
                    LEFT_HAND_TRANSLATION_Y.set(0.0D);
                    LEFT_HAND_TRANSLATION_Z.set(0.0D);
                    LEFT_HAND_ROTATION_X.set(0.0D);
                    LEFT_HAND_ROTATION_Y.set(0.0D);
                    LEFT_HAND_ROTATION_Z.set(0.0D);
                    LEFT_HAND_SCALE_X.set(1.0D);
                    LEFT_HAND_SCALE_Y.set(1.0D);
                    LEFT_HAND_SCALE_Z.set(1.0D);
                }
                else if (side == HandSide.RIGHT)
                {
                    RIGHT_HAND_TRANSLATION_X.set(0.0D);
                    RIGHT_HAND_TRANSLATION_Y.set(0.0D);
                    RIGHT_HAND_TRANSLATION_Z.set(0.0D);
                    RIGHT_HAND_ROTATION_X.set(0.0D);
                    RIGHT_HAND_ROTATION_Y.set(0.0D);
                    RIGHT_HAND_ROTATION_Z.set(0.0D);
                    RIGHT_HAND_SCALE_X.set(1.0D);
                    RIGHT_HAND_SCALE_Y.set(1.0D);
                    RIGHT_HAND_SCALE_Z.set(1.0D);
                }
            }
        }
    }

    enum Transforms
    {
        TRANSLATION,
        ROTATION,
        SCALE
    }

    enum Modes
    {
        ITEM,
        HAND;

        public Modes revert()
        {
            if (this == ITEM)
                return HAND;
            return ITEM;
        }

        @Override
        public String toString()
        {
            return this == ITEM ? "items" : "hands";
        }
    }
}
