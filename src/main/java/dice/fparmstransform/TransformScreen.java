package poopoodice.fparmstransform;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.AbstractGui;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.util.HandSide;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;

import static org.lwjgl.glfw.GLFW.*;
import static poopoodice.fparmstransform.Config.*;

public class TransformScreen extends Screen
{
    protected TransformScreen()
    {
        super(new StringTextComponent(""));
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks)
    {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        if (!INFO)
            return;
        Minecraft minecraft = Minecraft.getInstance();
        AbstractGui.drawCenteredString(matrixStack, font, "Current transform type: " + TRANSFORM.toString().toLowerCase(), (int) (minecraft.getMainWindow().getScaledWidth() / 2.0F), (int) (minecraft.getMainWindow().getScaledHeight() / 4.0F * 3.0F), 0xFFDFDFDF);
        AbstractGui.drawCenteredString(matrixStack, font, "Current mode: " + MODE.toString(), (int) (minecraft.getMainWindow().getScaledWidth() / 2.0F), (int) (minecraft.getMainWindow().getScaledHeight() / 4.0F * 3.0F) + 7, 0xFFDFDFDF);
        if (MODE == Modes.HAND)
        {
            String hand;
            HandSide h1 = HANDS[0];
            HandSide h2 = HANDS[1];
            if (h1 == null)
            {
                if (h2 == null)
                    hand = "empty";
                else
                    hand = h2.toString();
            }
            else
            {
                if (h2 == null)
                    hand = h1.toString();
                else
                    hand = "both";
            }
            AbstractGui.drawCenteredString(matrixStack, font, "Current hand: " + hand.toLowerCase(), (int) (minecraft.getMainWindow().getScaledWidth() / 2.0F), (int) (minecraft.getMainWindow().getScaledHeight() / 4.0F * 3.0F) + 14, 0xFFDFDFDF);
        }
        else
        {
            String hand;
            if (!LEFT_ITEM)
            {
                if (!RIGHT_ITEM)
                    hand = "empty";
                else
                    hand = "right";
            }
            else
            {
                if (!RIGHT_ITEM)
                    hand = "left";
                else
                    hand = "both";
            }
            AbstractGui.drawCenteredString(matrixStack, font, "Current selected item side: " + hand.toLowerCase(), (int) (minecraft.getMainWindow().getScaledWidth() / 2.0F), (int) (minecraft.getMainWindow().getScaledHeight() / 4.0F * 3.0F) + 14, 0xFFDFDFDF);
        }
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int modifiers)
    {
        if (keyCode == GLFW_KEY_ESCAPE)
            ENABLED.set(false);
        if (!ENABLED.get())
            closeScreen();
        boolean shift = hasShiftDown();
        boolean handMode = MODE == Modes.HAND;
        boolean w = keyCode == GLFW_KEY_W;
        boolean a = keyCode == GLFW_KEY_A;
        boolean s = keyCode == GLFW_KEY_S;
        boolean d = keyCode == GLFW_KEY_D;
        boolean q = keyCode == GLFW_KEY_Q;
        boolean e = keyCode == GLFW_KEY_E;
        if (keyCode == GLFW_KEY_DELETE)
            setToDefault(handMode ? HANDS : null);
        if (keyCode == GLFW_KEY_1)
            TRANSFORM = Transforms.TRANSLATION;
        else if (keyCode == GLFW_KEY_2)
            TRANSFORM = Transforms.ROTATION;
        else if (keyCode == GLFW_KEY_3)
            TRANSFORM = Transforms.SCALE;
        else if (keyCode == GLFW_KEY_Z)
        {
            if (handMode)
            {
                if (HANDS[0] == null)
                    HANDS[0] = HandSide.LEFT;
                else
                {
                    if (HANDS[1] != null)
                        HANDS[1] = null;
                    else
                        HANDS[0] = null;
                }
            }
            else
            {
                if (!LEFT_ITEM)
                    LEFT_ITEM = true;
                else
                {
                    if (RIGHT_ITEM)
                        RIGHT_ITEM = false;
                    else
                        LEFT_ITEM = false;
                }
            }
        }
        else if (keyCode == GLFW_KEY_X)
        {
            if (handMode)
            {
                if (HANDS[1] == null)
                    HANDS[1] = HandSide.RIGHT;
                else
                {
                    if (HANDS[0] != null)
                        HANDS[0] = null;
                    else
                        HANDS[1] = null;
                }
            }
            else
            {
                if (!RIGHT_ITEM)
                    RIGHT_ITEM = true;
                else
                {
                    if (LEFT_ITEM)
                        LEFT_ITEM = false;
                    else
                        RIGHT_ITEM = false;
                }
            }
        }
        else if (keyCode == GLFW_KEY_C)
        {
            if (handMode)
            {
                if (HANDS[0] == null || HANDS[1] == null)
                {
                    HANDS[0] = HandSide.LEFT;
                    HANDS[1] = HandSide.RIGHT;
                }
                else
                {
                    HANDS[0] = null;
                    HANDS[1] = null;
                }
            }
            else
            {
                if (!LEFT_ITEM || !RIGHT_ITEM)
                {
                    LEFT_ITEM = true;
                    RIGHT_ITEM = true;
                }
                else
                {
                    LEFT_ITEM = false;
                    RIGHT_ITEM = false;
                }
            }
        }
        else if (keyCode == GLFW_KEY_I)
            INFO = !INFO;
        else if (keyCode == GLFW_KEY_M)
        {
            MODE = MODE.revert();
            if (MODE == Modes.HAND)
            {
                LEFT_ITEM = false;
                RIGHT_ITEM = false;
            }
            else
            {
                HANDS[0] = null;
                HANDS[1] = null;
            }
        }
        HandSide[] handSides = handMode ? HANDS : null;
        switch (TRANSFORM)
        {
            case TRANSLATION:
                double amount = 0.025D;
                if (shift)
                {
                    if (w)
                        addTranslation(handSides, 0.0F, amount, 0.0F);
                    else if (s)
                        addTranslation(handSides, 0.0F, -amount, 0.0F);
                }
                else
                {
                    if (w)
                        addTranslation(handSides, 0.0F, 0.0F, -amount);
                    else if (s)
                        addTranslation(handSides, 0.0F, 0.0F, amount);
                    else if (a)
                        addTranslation(handSides, amount * (handMode ? 1.0F : -1.0F), 0.0F, 0.0F);
                    else if (d)
                        addTranslation(handSides, -amount * (handMode ? 1.0F : -1.0F), 0.0F, 0.0F);
                }
                break;
            case ROTATION:
                amount = 1.0F;
                if (handMode)
                {
                    if (w)
                        addRotation(handSides, -amount, 0.0F, 0.0F);
                    else if (s)
                        addRotation(handSides, amount, 0.0F, 0.0F);
                    else if (a)
                        addRotation(handSides, 0.0F, 0.0F, -amount);
                    else if (d)
                        addRotation(handSides, 0.0F, 0.0F, amount);
                    else if (q)
                        addRotation(handSides, 0.0F, -amount, 0.0F);
                    else if (e)
                        addRotation(handSides, 0.0F, amount, 0.0F);
                }
                else
                {
                    if (w)
                        addRotation(null, amount, 0.0F, 0.0F);
                    else if (s)
                        addRotation(null, -amount, 0.0F, 0.0F);
                    else if (a)
                        addRotation(null, 0.0F, amount, 0.0F);
                    else if (d)
                        addRotation(null, 0.0F, -amount, 0.0F);
                    else if (q)
                        addRotation(null, 0.0F, 0.0F, amount);
                    else if (e)
                        addRotation(null, 0.0F, 0.0F, -amount);
                }
                break;
            case SCALE:
                amount = 0.025D;
                if (handMode)
                {
                    if (q)
                        addScale(handSides, amount, 0.0F, 0.0F);
                    else if (w)
                        addScale(handSides, 0.0F, amount, 0.0F);
                    else if (e)
                        addScale(handSides, 0.0F, 0.0F, amount);
                    else if (a)
                        addScale(handSides, -amount, 0.0F, 0.0F);
                    else if (s)
                        addScale(handSides, 0.0F, -amount, 0.0F);
                    else if (d)
                        addScale(handSides, 0.0F, 0.0F, -amount);
                }
                else
                {
                    if (q)
                        addScale(null, amount, 0.0F, 0.0F);
                    else if (w)
                        addScale(null, 0.0F, 0.0F, amount);
                    else if (e)
                        addScale(null, 0.0F, amount, 0.0F);
                    else if (a)
                        addScale(null, -amount, 0.0F, 0.0F);
                    else if (s)
                        addScale(null, 0.0F, 0.0F, -amount);
                    else if (d)
                        addScale(null, 0.0F, -amount, 0.0F);
                }
                break;
            default:
                return false;
        }
        LEFT_HAND_PERSPECTIVE.set(getVectorString(getRotation(HandSide.LEFT), getTranslation(HandSide.LEFT), getScale(HandSide.LEFT)));
        LEFT_HAND_ITEM_PERSPECTIVE.set(getVectorString(getItemRotation(HandSide.LEFT), getItemTranslation(HandSide.LEFT), getItemScale(HandSide.LEFT)));
        RIGHT_HAND_PERSPECTIVE.set(getVectorString(getRotation(HandSide.RIGHT), getTranslation(HandSide.RIGHT), getScale(HandSide.RIGHT)));
        RIGHT_HAND_ITEM_PERSPECTIVE.set(getVectorString(getItemRotation(HandSide.RIGHT), getItemTranslation(HandSide.RIGHT), getItemScale(HandSide.RIGHT)));
        return true;
    }

    private static String getVectorString(Vector3d vec1, Vector3d vec2, Vector3d vec3)
    {
        return (vec1.toString().replace(')', ',') + vec2.toString().replace('(', ' ').replace(')', ',') + vec3.toString().replace('(', ' ').replaceAll("\\)", "F)")).replaceAll(",", "F,");
    }

    @Override
    public boolean isPauseScreen()
    {
        return false;
    }
}
